<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'tempo_expiracao_senha','login', 'cod_autorizacao', 'status_usuario', 'cod_pessoa'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $dates = ['deleted_at'];


    public function profiles()
    {
        return $this->belongsToMany(Profile::class, 'user_profile', 'user_id', 'profile_id');
    }

    public function gadgets()
    {
        return $this->belongsToMany(Profile::class, 'user_gadgets', 'user_id', 'gadgets_id');
    }
}
