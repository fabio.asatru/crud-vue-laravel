<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 23/07/19
 * Time: 19:02
 */

namespace App\Service;


use App\Gadgets;

class GadgetsService
{

    private $gadgets;

    public function __construct(Gadgets $gadgets)
    {
        $this->gadgets = $gadgets;
    }

    public function list()
    {
        return $this->gadgets->get();
    }

}
