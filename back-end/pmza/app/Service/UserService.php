<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 22/07/19
 * Time: 20:22
 */

namespace App\Service;


use App\Gadgets;
use App\Profile;
use App\User;
use Illuminate\Http\Request;

class UserService
{
    private $user;
    private $profile;
    private $gadgets;

    public function __construct(User $user, Profile $profile, Gadgets $gadgets)
    {
        $this->user = $user;
        $this->profile = $profile;
        $this->gadgets = $gadgets;
    }

    public function listar()
    {
        return  $this->user->orderBy('created_at', 'desc')->get()->load('profiles', 'gadgets');
    }

    public function show($id)
    {
        return $this->user->find($id)->load('profiles', 'gadgets');
    }

    public function store(Request $request)
    {
        $data =  $request->all();
        $user = $this->user->create($data);

        if($user){
            $profiles = $this->profile->find($data['profiles']);
            $user->profiles()->attach($profiles);

            $gadgets = $this->gadgets->find($data['gadgets']);
            $user->gadgets()->attach($gadgets);

            return response()->json($user, 200);
        }
    }

    public function update(Request $request, $id){

        try{
            $data = $request->all();
            $user = $this->user->find($id);

            $user->update($data);

            $user->profiles()->sync($data['profiles']);
            $user->gadgets()->sync($data['gadgets']);

            return response()->json(['msg' => 'Usuário Excluido Com Sucesso'], 200);

        }catch (\Throwable $ex) {

            return response()->json(['msg' => 'Nāo Foi Possivel Excluir Usuário'], 500);
        }



    }
}
